<?php

namespace Drupal\url_text\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'url' entity field type.
 *
 * @FieldType(
 *   id = "url",
 *   label = @Translation("URL"),
 *   description = @Translation("A field containing a plain string URL value."),
 *   default_widget = "url_textfield",
 *   default_formatter = "url"
 * )
 */
class UrlItem extends FieldItemBase {

   /**
   * The maximum length for a telephone value.
   */
  const MAX_LENGTH = 256;

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {

    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => self::MAX_LENGTH,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
 }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {

    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Value'))
      ->setRequired(FALSE);

    return $properties;
  }

    /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    $constraints[] = $constraint_manager->create('ComplexData', [
      'value' => [
        'Length' => [
          'max' => self::MAX_LENGTH,
          'maxMessage' => $this->t('%name: the URL may not be longer than @max characters.', ['%name' => $this->getFieldDefinition()->getLabel(), '@max' => self::MAX_LENGTH]),
        ],
      ],
    ]);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = 'https://example.com';
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    return [
      'allowed_schemes' => ['http', 'https'],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {

    $settings = $this->getSettings();

    $element['allowed_schemes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed Schemes'),
      '#options' => $this->getSchemes(),
      '#default_value' => $settings['allowed_schemes'] ?? ['http', 'https'],
      '#description' => $this->t('Allowed URL schemes for the URL. If none are selected, then all are allowed.'),
    ];

    return $element;
  }

  /**
   * Provide an array of URL schemes to allow.
   */
  public static function getSchemes(): array {

    return [
      'http' => 'http://',
      'https' => 'https://',
      'ftp' => 'ftp://',
      'mailto' => 'mailto://',
      'sftp' => 'sftp://',
      'ssh' => 'ssh://',
      'tel' => 'tel://',
    ];
  }

}
