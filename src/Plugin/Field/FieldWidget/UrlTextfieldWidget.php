<?php

namespace Drupal\url_text\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\url_text\Plugin\Field\FieldType\UrlItem;

/**
 * Plugin implementation of the 'url' widget.
 */
#[FieldWidget(
  id: "url_textfield",
  label: new TranslatableMarkup("URL"),
  field_types: ["url"],
 )]

class UrlTextfieldWidget extends WidgetBase {


  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * Hold the allowed URL schemes.
   *
   * @var array
   */
  public static array $schemes;

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];

    $placeholder = $this->getSetting('placeholder');
    if (!empty($placeholder)) {
      $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $placeholder]);
    }
    else {
      $summary[] = $this->t('No placeholder');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    static::$schemes = $this->getSchemes($items);

    $element = $element + [
      '#title' => $this->t('URL'),
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => UrlItem::MAX_LENGTH,
      '#attributes' => ['class' => ['url-text']],
      '#element_validate' => [[static::class, 'validate']],
    ];

    // Add desc here because array + will not overwrite the default.
    $element['#description'] = $this->t('Enter a URL. Allowed schemes are: @desc.', ['@desc' => $this->createDescription()]);

    return ['value' => $element];
  }

  /**
   * Validate the scheme selection and format.
   */
  public static function validate($element, FormStateInterface $form_state): void {
    $value = $element['#value'];
    if ($value) {
      // Find the end of the scheme text.
      $pos = strpos($value, '://');
      if ($pos !== FALSE) {
        // Get the full scheme.
        $scheme = substr($value, 0, $pos + 3);
        // Get just the text portion of the scheme.
        $scheme_type = substr($value, 0, $pos);
        // Verify that the scheme is enabled for this field.
        if (!in_array($scheme_type, static::$schemes)) {
          $form_state->setError($element, t('@scheme_type is not an enabled URL scheme.', ['@scheme_type' => $scheme_type]));
        }

        // Verify that the full text of the field matches the scheme format.
        $preg_match = static::testRegex($value, $scheme_type);
        if (!$preg_match) {
          $form_state->setError($element, t('Invalid format for @scheme.', ['@scheme' => $scheme]));
        }
      }
      else {
          $form_state->setError($element, t('Invalid format.'));
      }
    }
  }

  /**
   * Generate the URL text field description based on allowed schemes.
   */
  private function createDescription(): string {

    return implode(', ', UrlTextfieldWidget::$schemes);
  }

  /**
   * Retrieve the allowed schemes from the field settings.
   */
  private function getSchemes(FieldItemListInterface $items): array {
    $def = $items->getFieldDefinition();
    $schemes = $def->getSetting('allowed_schemes');

    $enabled = [];
    // Iterate the allowed schemes to return those selected.
    foreach ($schemes as $scheme) {
      if ($scheme) {
        $enabled[] = $scheme;
      }
    }

    return $enabled;
  }

  /**
   * Validate the field text against the chosen scheme's pattern.
   */
  private static function testRegex($value, $scheme): array {
    $pattern = [
      'http' => '/^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/',
      'https' => '/^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/',
      'ftp' => '%^ftp:\/\/(([a-z0-9]+):([a-z0-9]+)@)*([\\.a-z0-9]+)/([\\./a-z0-9]+)$%',
      'sftp' => '%^sftp:\/\/(([a-z0-9]+):([a-z0-9]+)@)*([\\.a-z0-9]+)/([\\./a-z0-9]+)$%',
      'ssh' => '%^ssh:\/\/(([a-z0-9]+):([a-z0-9]+)@)*([\\.a-z0-9]+)/([\\./a-z0-9]+)$%',
      'mailto' => '/^mailto:\/\/[\w\.\-]+@([\w\-]+\.)+[\w\-]{2,4}$/',
      'tel' => '^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$',
    ];

    preg_match($pattern[$scheme], $value, $matches);
    return $matches;
  }

}
