# URL Text

URL Text provides a custom field type to hold a URL in plain text. The field
is validated to contain a valid URL.


## Requirements

This module has no specific requirements.


## Recommended Modules

This field type does **NOT** provide a link. For that, see the Link field
type in core or the [Linkit](https://www.drupal.org/project/linkit) module.


## Features

A custom field type that displays as a text box into which is typed a URL,
which will be validated as containing a well-formed URL value, based on
the chosen URL scheme.


### Supported URL schemes are:

- http
- https
- ftp
- sftp
- mailto
- ssh
- tel


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.


## Using the Module

Once enabled, the field type
list when adding a field to a content or paragraph type will contain an
entry for URL.


## Maintainers

- Jeff Greenberg - [jAyenGreen](https://www.drupal.org/u/j-ayen-green)
- Wes Fairhead - [wfairhead](https://www.drupal.org/u/wfairhead)
